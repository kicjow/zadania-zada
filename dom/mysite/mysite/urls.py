from django.conf.urls import patterns, include, url
from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse
import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()
urlpatterns = patterns('',
     url(r'^admin/', include(admin.site.urls)),
     url(r'^gallery/', include('gallery.urls')),
     url(r'^$', include('gallery.urls')),
     url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
     url(r'^static/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.STATICFILES_DIRS[0], 'show_indexes': True}),
     url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
)